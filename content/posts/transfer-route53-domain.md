---
title: "How to Transfer a Route 53 Domain Between AWS Accounts"
date: 2020-12-04T21:51:22+01:00
draft: false
categories:
- AWS
tags:
- aws
- dns
ShowToc: true
TocOpen: true
---
Sometimes it is needed to transfer a Route 53 domain to another AWS account, and users [can do it](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-transfer-between-aws-accounts.html) either by contacting AWS Support or manually using AWS API. This article describes how to make a transfer with AWS CLI.

## Transfer a domain

Request a transfer from the account that currently has a registered domain:

```bash
aws --profile old_profile \
    --region us-east-1 \
    route53domains transfer-domain-to-another-aws-account \
    --domain-name example.com \
    --account-id your-target-account-id
```

You will get an output with operation id and password required to confirm the transfer:

```json
{
    "OperationId": "some-id-1",
    "Password": "qwerty"
}
```

Also, Route 53 Management Console will now show a notification in both accounts about domain transfer being in progress. Then you can proceed to the transfer verification on a target account's side:

```bash
aws --profile new_profile \
    --region us-east-1 \
    route53domains accept-domain-transfer-from-another-aws-account \
    --domain-name example.com \
    --password qwerty
```

You will get the following response:

```json
{
    "OperationId": "some-id-2"
}
```

You can check a transfer status either by verifying notifications in the Route 53 Management Console or by checking an operation id:

```bash
aws --profile new_profile \
    --region us-east-1 \
    route53domains get-operation-detail \
    --operation-id some-id-2
```

Which should show the following response:

```json
{
    "OperationId": "some-id-2",
    "Status": "SUCCESSFUL",
    "DomainName": "example.com",
    "Type": "INTERNAL_TRANSFER_IN_DOMAIN",
    "SubmittedDate": 1234.123
}
```

## Migrate hosted zones

AWS does not automatically migrate your hosted zones nor allows a zone export using Management Console, but this can be easily done with the [cli53](https://github.com/barnybug/cli53) utility:

```bash
cli53 --full \
      --profile old_profile \
      example.com
```

Now you can create a zone `example.com` via Route 53 Management Console, click the 'Import Zone File' button and paste the whole `cli53`'s output. AWS ignores `SOA`/`NS` records and can import up to 1000 records, see all notes in the [Route 53 documentation](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/resource-record-sets-creating-import.html).

## Update name servers

The final step is to update name servers in the account, where your domain was transfered. For this go to the hosted zone you have created in the previous section, copy the `NS` record's value, select your registered domain in the Route 53 Dashboard (https://console.aws.amazon.com/route53/home#DomainListing:), and replace name servers.

This might take some time, and you will receive an email when updated name servers were propagated.
