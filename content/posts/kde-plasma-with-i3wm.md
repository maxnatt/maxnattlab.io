---
title: "KDE Plasma with i3 WM Instead of KWin"
date: 2022-06-27T20:56:00+01:00
draft: false
categories:
- Linux
- Eye candy
tags:
- plasma
- i3
ShowToc: true
TocOpen: true
cover:
  image: plasma-i3-desktop.jpg
---
**Update [June 27, 2022]**: describe KDE 5.25 systemd-related changes.

The advantages of desktop environments over plain window managers on Linux are clear. Normally, one gets almost everything he can think of straight out of the box, including power management, applets, sound configuration, notifications, quick settings, eye candy, and so on.

Tiling or dynamic windows managers, on the other hand, normally do not offer any of those features out of the box and need some time for configuration, but they deliver a very fast and convenient keyboard-driven way to manage your windows.

This article explains how to combine one of the most popular solutions of both worlds - i3/i3-gaps and KDE Plasma.

## Preparation

Make sure you have installed packages needed for minimal install, e.g. i3/i3-gaps and [picom](https://github.com/yshui/picom). 

Also, i3 config needs some general and Plasma-specific rules to be added:

```bash
# >>> Plasma Integration <<<
  # Try to kill the wallpaper set by Plasma (it takes up the entire workspace and hides everything)
  
  exec --no-startup-id wmctrl -c Plasma
  for_window [title="Desktop — Plasma"] kill; floating enable; border none
  
  # Compositor (Animations, Shadows, Transparency)
  exec --no-startup-id picom -cCFb

# >>> Window rules <<<
  # >>> Avoid tiling Plasma popups, dropdown windows, etc. <<<
  # For the first time, manually resize them, i3 will remember the setting for floating windows
    for_window [class="yakuake"] floating enable;
    for_window [class="lattedock"] floating enable;
    for_window [class="plasmashell"] floating enable;
    for_window [class="Kmix"] floating enable; border none
    for_window [class="kruler"] floating enable; border none
    for_window [class="Plasma"] floating enable; border none
    for_window [class="Klipper"] floating enable; border none
    for_window [class="krunner"] floating enable; border none
    for_window [class="Plasmoidviewer"] floating enable; border none
    for_window [title="plasma-desktop"] floating enable; border none
    for_window [class="plasmashell" window_type="notification"] floating enable, border none, move position 1450px 20px
    no_focus [class="plasmashell" window_type="notification"] 

  # >>> Avoid tiling for non-Plasma stuff <<<
    for_window [window_role="pop-up"] floating enable
    for_window [window_role="bubble"] floating enable
    for_window [window_role="task_dialog"] floating enable
    for_window [window_role="Preferences"] floating enable
    for_window [window_role="About"] floating enable
    for_window [window_type="dialog"] floating enable
    for_window [window_type="menu"] floating enable
    for_window [instance="__scratchpad"] floating enable
```

Tips: 

1. Line `for_window [title="Desktop — Plasma"] kill; floating enable; border none` might need adjustments if you are using a language other than English, see [KDE Userbase](https://userbase.kde.org/Tutorials/Using_Other_Window_Managers_with_Plasma#Configure_i3).
1. Notifications might behave in a weird way so it is best to define a positioning. For FullHD one can use e.g. `for_window [class="plasmashell" window_type="notification"] floating enable, border none, move position 1450px 20px` (as in the config above), and QHD may use e.g. `for_window [class="plasmashell" window_type="notification"] floating enable, border none, move position 2050px 35px`.

## KDE 5.25 and newer

Since KDE 5.25, [systemd is used](https://blog.davidedmundson.co.uk/blog/plasma-and-the-systemd-startup/) to start Plasma by default. To use a custom WM, one needs to create own systemd service and mask the old one.

Mask the default service:

```bash
systemctl --user mask plasma-kwin_x11.service
```

Create a custom service at `~/.config/systemd/user/plasma-i3.service`:

```ini
[Install]
WantedBy=plasma-workspace.target

[Unit]
Description=Plasma Custom Window Manager
Before=plasma-workspace.target

[Service]
ExecStart=/usr/bin/i3
Slice=session.slice
Restart=on-failure
```

Enable the new service:

```bash
systemctl --user daemon-reload
systemctl --user enable plasma-i3.service
```

{{< notice tip >}}
If you are not able to use the new systemd-based startup, it is also currently possible to switch back to the script-based startup by running the following command and following KDE 5.24 instructions below:

```bash
kwriteconfig5 --file startkderc --group General --key systemdBoot false
```
{{< /notice >}}

## KDE 5.24 and older

Plasma relies on the `KDEWM` environment variable when using the legacy script-based startup.

### Not using a DM?

In case you are not using a display manager and/or want to enable i3 just for one user, you can create a `~/.config/plasma-workspace/env/wm.sh` script (Plasma will load it automatically) and make it executable:

```bash
#!/bin/sh
export KDEWM=/usr/bin/i3
```

Then you can proceed directly to the [Summary](#summary).

### Create a session entry

Here we will create a separate desktop session, so one could easily switch between a normal Plasma session and an i3-enabled one in a display manager. Create a file `/usr/share/xsessions/kde-with-i3.desktop`:

```
[Desktop Entry]
Name=kde-i3
Comment=KDE with i3
Exec=/usr/local/sbin/kde-i3
TryExec=/usr/local/sbin/kde-i3
Type=Application
X-LightDM-DesktopName=kde-i3
DesktopNames=kde-i3
Keywords=tiling;wm;windowmanager;window;manager;kde;
```

### Load i3 instead of KWin

Create a script `/usr/local/sbin/kde-i3` which configures Plasma to load i3 instead of KWin:

```bash
#!/bin/sh
export KDEWM=/usr/bin/i3
exec /usr/bin/startplasma-x11
```

Make it executable:

```bash
chmod a+x /usr/local/sbin/kde-i3
```

## Summary

At this moment you can log out or reboot to enter your new desktop session with Plasma + i3, and proceed to tune the i3, picom, conky, and so on. You can find my dotfiles at [https://gitlab.com/maxnatt/dotfiles](https://gitlab.com/maxnatt/dotfiles).
